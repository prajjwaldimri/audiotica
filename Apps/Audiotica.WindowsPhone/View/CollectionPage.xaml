﻿<audiotica:PageBase
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:audiotica="using:Audiotica"
    xmlns:interactivity="using:Microsoft.Xaml.Interactivity" xmlns:core="using:Microsoft.Xaml.Interactions.Core"
    Name="Page"
    x:Class="Audiotica.View.CollectionPage"
    mc:Ignorable="d"
    Background="{ThemeResource ApplicationPageBackgroundThemeBrush}"
    DataContext="{Binding Collection, Source={StaticResource Locator}}">
    <Grid>
        <Grid.Resources>
            <CollectionViewSource x:Key="SongGroups"
                                  IsSourceGrouped="True" Source="{Binding SortedSongs}" />
            <CollectionViewSource x:Key="ArtistGroups"
                                  IsSourceGrouped="True" Source="{Binding SortedArtists}" />
            <CollectionViewSource x:Key="AlbumGroups"
                                  IsSourceGrouped="True" Source="{Binding SortedAlbums}" />
        </Grid.Resources>

        <GridView ItemsSource="{Binding RandomizeAlbumList}">
            <GridView.ItemTemplate>
                <DataTemplate>
                    <Image
                        Width="{Binding DataContext.ArtworkSize, ElementName=Page}"
                        Height="{Binding DataContext.ArtworkSize, ElementName=Page}"
                        Opacity=".2"
                        Source="{Binding Artwork}" />
                </DataTemplate>
            </GridView.ItemTemplate>
        </GridView>

        <Pivot Name="CollectionPivot" x:Uid="CollectionPivot" Title="COLLECTION"
               HeaderTemplate="{StaticResource PivotHeaderTemplate}"
               SelectionChanged="CollectionPivot_SelectionChanged">
            <PivotItem x:Uid="PivotSongs" Header="SONGS">
                <Grid>
                    <TextBlock
                        x:Uid="CollectionEmpty"
                        Visibility="{Binding Service.Songs.Count, Converter={StaticResource EmptyListToVisibilityConverter}}"
                        Text="It seems you haven't saved any music yet." Style="{StaticResource SubtleTextBlockStyle}" />
                    <SemanticZoom>
                        <SemanticZoom.ZoomedInView>
                            <ListView IsItemClickEnabled="True"
                                      ItemsSource="{Binding Source={StaticResource SongGroups}}"
                                      Padding="0,0,0,25">
                                <ListView.ItemTemplate>
                                    <DataTemplate>
                                        <Grid Margin="0,0,0,15">
                                            <Grid.RowDefinitions>
                                                <RowDefinition Height="*" />
                                                <RowDefinition Height="Auto" />
                                            </Grid.RowDefinitions>
                                            <Grid>
                                                <Grid.ColumnDefinitions>
                                                    <ColumnDefinition Width="*" />
                                                    <ColumnDefinition Width="Auto" />
                                                </Grid.ColumnDefinitions>
                                                <interactivity:Interaction.Behaviors>
                                                    <core:EventTriggerBehavior EventName="Holding">
                                                        <audiotica:ShowFlyoutAction />
                                                    </core:EventTriggerBehavior>
                                                </interactivity:Interaction.Behaviors>
                                                <FlyoutBase.AttachedFlyout>
                                                    <MenuFlyout>
                                                        <MenuFlyoutItem x:Uid="MenuItemAddTo" Text="Add to"
                                                                        CommandParameter="{Binding}"
                                                                        Command="{Binding DataContext.AddToClickCommand, ElementName=Page}">
                                                            <interactivity:Interaction.Behaviors>
                                                                <core:EventTriggerBehavior EventName="Click">
                                                                    <audiotica:ShowFlyoutAction />
                                                                </core:EventTriggerBehavior>
                                                            </interactivity:Interaction.Behaviors>
                                                            <FlyoutBase.AttachedFlyout>
                                                                <ListPickerFlyout x:Name="Picker"
                                                                                  DisplayMemberPath="Name"
                                                                                  ItemsSource="{Binding AddableTo}">
                                                                    <interactivity:Interaction.Behaviors>
                                                                        <core:EventTriggerBehavior
                                                                            EventName="ItemsPicked">
                                                                            <core:InvokeCommandAction
                                                                                Command="{Binding DataContext.ItemPickedCommand, ElementName=Page}"
                                                                                CommandParameter="{Binding SelectedItem, ElementName=Picker}" />
                                                                        </core:EventTriggerBehavior>
                                                                    </interactivity:Interaction.Behaviors>
                                                                </ListPickerFlyout>
                                                            </FlyoutBase.AttachedFlyout>
                                                        </MenuFlyoutItem>
                                                        <MenuFlyoutItem x:Uid="MenuItemDelete" Text="Delete"
                                                                        Foreground="Red"
                                                                        CommandParameter="{Binding}"
                                                                        Command="{Binding DataContext.DeleteClickCommand, ElementName=Page}" />
                                                    </MenuFlyout>
                                                </FlyoutBase.AttachedFlyout>
                                                <StackPanel>
                                                    <TextBlock Text="{Binding Name}" FontSize="30"
                                                               FontFamily="Global User Interface" TextWrapping="Wrap"
                                                               TextTrimming="CharacterEllipsis" FontWeight="Bold" />
                                                    <TextBlock FontSize="22" FontWeight="Thin"
                                                               Foreground="#89FFFFFF" TextTrimming="CharacterEllipsis"
                                                               Margin="0,-5,0,0" TextWrapping="Wrap">
                                                        <Run Text="{Binding Artist.Name}" />
                                                        <Run Text="-" />
                                                        <Run Text="{Binding Album.Name}" />
                                                    </TextBlock>
                                                </StackPanel>
                                                <Grid Grid.Column="1"
                                                      Visibility="{Binding SongState, Converter={StaticResource NoneSongStateConverter}}">
                                                    <AppBarButton Icon="Download" IsCompact="True" Margin="0,-5,0,0"
                                                                  Command="{Binding DataContext.DownloadClickCommand ,ElementName=Page}"
                                                                  CommandParameter="{Binding}" />
                                                </Grid>
                                                <Grid Grid.Column="1"
                                                      Visibility="{Binding SongState, Converter={StaticResource DownloadingSongStateConverter}}">
                                                    <AppBarButton Icon="Cancel" IsCompact="True" Margin="0,-5,0,0"
                                                                  Command="{Binding DataContext.CancelClickCommand ,ElementName=Page}"
                                                                  CommandParameter="{Binding}" />
                                                </Grid>
                                            </Grid>
                                            <Grid Grid.Row="1"
                                                  Visibility="{Binding SongState, Converter={StaticResource DownloadingSongStateConverter}}"
                                                  Margin="0,10,0,0">
                                                <ProgressBar Value="{Binding Download.BytesReceived, Mode=TwoWay}"
                                                             Maximum="{Binding Download.BytesToReceive, Mode=TwoWay}" />
                                            </Grid>
                                        </Grid>
                                    </DataTemplate>
                                </ListView.ItemTemplate>
                                <ListView.GroupStyle>
                                    <GroupStyle HidesIfEmpty="True"
                                                HeaderTemplate="{StaticResource GroupHeaderTemplate}" />
                                </ListView.GroupStyle>
                                <interactivity:Interaction.Behaviors>
                                    <core:EventTriggerBehavior EventName="ItemClick">
                                        <core:InvokeCommandAction Command="{Binding SongClickCommand}" />
                                    </core:EventTriggerBehavior>
                                </interactivity:Interaction.Behaviors>
                            </ListView>
                        </SemanticZoom.ZoomedInView>
                        <SemanticZoom.ZoomedOutView>
                            <GridView Background="#BF000000" Padding="15,10,0,0"
                                      ItemsSource="{Binding Source={StaticResource SongGroups}, Path=CollectionGroups}"
                                      ItemTemplate="{StaticResource JumpTemplate}" />
                        </SemanticZoom.ZoomedOutView>
                    </SemanticZoom>
                </Grid>
            </PivotItem>
            <PivotItem x:Uid="PivotArtists" Header="ARTISTS">
                <Grid>
                    <TextBlock
                        x:Uid="CollectionEmpty"
                        Visibility="{Binding Service.Artists.Count, Converter={StaticResource EmptyListToVisibilityConverter}}"
                        Text="It seems you haven't saved any music yet." Style="{StaticResource SubtleTextBlockStyle}" />
                    <SemanticZoom>
                        <SemanticZoom.ZoomedInView>
                            <GridView
                                IsItemClickEnabled="True"
                                ItemsSource="{Binding Source={StaticResource ArtistGroups}}"
                                Padding="0,0,0,25">
                                <interactivity:Interaction.Behaviors>
                                    <core:EventTriggerBehavior EventName="ItemClick">
                                        <core:InvokeCommandAction Command="{Binding ArtistClickCommand}" />
                                    </core:EventTriggerBehavior>
                                </interactivity:Interaction.Behaviors>
                                <GridView.GroupStyle>
                                    <GroupStyle HidesIfEmpty="True"
                                                HeaderTemplate="{StaticResource GroupHeaderTemplate}" />
                                </GridView.GroupStyle>
                                <GridView.ItemTemplate>
                                    <DataTemplate>
                                        <StackPanel Margin="0,0,5,5" Width="115">
                                            <interactivity:Interaction.Behaviors>
                                                <core:EventTriggerBehavior EventName="Holding">
                                                    <audiotica:ShowFlyoutAction />
                                                </core:EventTriggerBehavior>
                                            </interactivity:Interaction.Behaviors>
                                            <FlyoutBase.AttachedFlyout>
                                                <MenuFlyout>
                                                    <MenuFlyoutItem x:Uid="MenuItemDelete" Text="Delete"
                                                                    Foreground="Red"
                                                                    Command="{Binding DataContext.DeleteClickCommand, ElementName=Page}"
                                                                    CommandParameter="{Binding}" />
                                                </MenuFlyout>
                                            </FlyoutBase.AttachedFlyout>
                                            <Grid Background="{StaticResource PhoneAccentBrush}" Margin="0,0,0,5">
                                                <Image Source="{Binding Artwork}" Height="115" Width="115"
                                                       Stretch="UniformToFill" />
                                                <AppBarButton Icon="Play" IsCompact="True" Margin="0,-10,10,0"
                                                              Command="{Binding DataContext.EntryPlayClickCommand, ElementName=Page}"
                                                              CommandParameter="{Binding}"
                                                              Style="{StaticResource AppBarButtonWithShadowStyle}" />
                                            </Grid>
                                            <TextBlock Text="{Binding Name}" FontSize="15"
                                                       TextTrimming="CharacterEllipsis"
                                                       FontFamily="Global User Interface" />
                                            <TextBlock FontSize="14"
                                                       TextTrimming="CharacterEllipsis" Foreground="#89FFFFFF"
                                                       FontFamily="Global User Interface">
                                                <Run Text="{Binding Albums.Count}" />
                                                <Run
                                                    Text="{Binding Albums.Count, Converter={StaticResource AlbumToPluralConverter}}" />
                                            </TextBlock>
                                        </StackPanel>
                                    </DataTemplate>
                                </GridView.ItemTemplate>
                            </GridView>
                        </SemanticZoom.ZoomedInView>
                        <SemanticZoom.ZoomedOutView>
                            <GridView Background="#BF000000" Padding="15,10,0,0"
                                      ItemsSource="{Binding Source={StaticResource ArtistGroups}, Path=CollectionGroups}"
                                      ItemTemplate="{StaticResource JumpTemplate}" />
                        </SemanticZoom.ZoomedOutView>
                    </SemanticZoom>
                </Grid>
            </PivotItem>
            <PivotItem x:Uid="PivotAlbums" Header="ALBUMS">
                <Grid>
                    <TextBlock
                        x:Uid="CollectionEmpty"
                        Visibility="{Binding Service.Albums.Count, Converter={StaticResource EmptyListToVisibilityConverter}}"
                        Text="It seems you haven't saved any music yet." Style="{StaticResource SubtleTextBlockStyle}" />
                    <SemanticZoom>
                        <SemanticZoom.ZoomedInView>
                            <GridView
                                IsItemClickEnabled="True"
                                ItemsSource="{Binding Source={StaticResource AlbumGroups}}"
                                Padding="0,0,0,25">
                                <interactivity:Interaction.Behaviors>
                                    <core:EventTriggerBehavior EventName="ItemClick">
                                        <core:InvokeCommandAction Command="{Binding AlbumClickCommand}" />
                                    </core:EventTriggerBehavior>
                                </interactivity:Interaction.Behaviors>
                                <GridView.ItemTemplate>
                                    <DataTemplate>
                                        <StackPanel Margin="0,0,5,5" Width="115">
                                            <interactivity:Interaction.Behaviors>
                                                <core:EventTriggerBehavior EventName="Holding">
                                                    <audiotica:ShowFlyoutAction />
                                                </core:EventTriggerBehavior>
                                            </interactivity:Interaction.Behaviors>
                                            <FlyoutBase.AttachedFlyout>
                                                <MenuFlyout>
                                                    <MenuFlyoutItem x:Uid="MenuItemDelete" Text="Delete" Foreground="Red"
                                                        Command="{Binding DataContext.DeleteClickCommand, ElementName=Page}"
                                                        CommandParameter="{Binding}" />
                                                </MenuFlyout>
                                            </FlyoutBase.AttachedFlyout>
                                            <Grid Background="{StaticResource PhoneAccentBrush}" Margin="0,0,0,5">
                                                <Image Source="{Binding Artwork}" Height="115" Width="115"
                                                       Stretch="UniformToFill" />
                                                <AppBarButton Icon="Play" IsCompact="True" Margin="0,-10,10,0"
                                                              Command="{Binding DataContext.EntryPlayClickCommand, ElementName=Page}"
                                                              CommandParameter="{Binding}"
                                                              Style="{StaticResource AppBarButtonWithShadowStyle}" />
                                            </Grid>
                                            <TextBlock Text="{Binding Name}" FontSize="15"
                                                       TextTrimming="CharacterEllipsis"
                                                       FontFamily="Global User Interface" />
                                            <TextBlock FontSize="14" Text="{Binding PrimaryArtist.Name}"
                                                       TextTrimming="CharacterEllipsis" Foreground="#89FFFFFF"
                                                       FontFamily="Global User Interface" />
                                            <TextBlock FontSize="14"
                                                       TextTrimming="CharacterEllipsis" Foreground="#89FFFFFF"
                                                       FontFamily="Global User Interface">
                                                <Run Text="{Binding Songs.Count}" />
                                                <Run
                                                    Text="{Binding Songs.Count, Converter={StaticResource SongToPluralConverter}}" />
                                            </TextBlock>
                                        </StackPanel>
                                    </DataTemplate>
                                </GridView.ItemTemplate>
                                <GridView.GroupStyle>
                                    <GroupStyle HidesIfEmpty="True"
                                                HeaderTemplate="{StaticResource GroupHeaderTemplate}" />
                                </GridView.GroupStyle>
                            </GridView>
                        </SemanticZoom.ZoomedInView>
                        <SemanticZoom.ZoomedOutView>
                            <GridView Background="#BF000000" Padding="15,10,0,0"
                                      ItemsSource="{Binding Source={StaticResource AlbumGroups}, Path=CollectionGroups}"
                                      ItemTemplate="{StaticResource JumpTemplate}" />
                        </SemanticZoom.ZoomedOutView>
                    </SemanticZoom>
                </Grid>
            </PivotItem>
            <PivotItem x:Uid="PivotPlaylists" Header="PLAYLIST">
                <Grid>
                    <TextBlock
                        x:Uid="CollectionNoPlaylist"
                        Visibility="{Binding Service.Playlists.Count, Converter={StaticResource EmptyListToVisibilityConverter}}"
                        Text="You haven't made any playlist yet. Try pressing the new button below."
                        Style="{StaticResource SubtleTextBlockStyle}" />
                    <ListView IsItemClickEnabled="True" ItemsSource="{Binding Service.Playlists}" Padding="0,0,0,25">
                        <interactivity:Interaction.Behaviors>
                            <core:EventTriggerBehavior EventName="ItemClick">
                                <core:InvokeCommandAction Command="{Binding PlaylistClickCommand}" />
                            </core:EventTriggerBehavior>
                        </interactivity:Interaction.Behaviors>
                        <ListView.ItemTemplate>
                            <DataTemplate>
                                <Grid Margin="0,0,0,15">
                                    <Grid.ColumnDefinitions>
                                        <ColumnDefinition Width="Auto" />
                                        <ColumnDefinition Width="*" />
                                    </Grid.ColumnDefinitions>
                                    <interactivity:Interaction.Behaviors>
                                        <core:EventTriggerBehavior EventName="Holding">
                                            <audiotica:ShowFlyoutAction />
                                        </core:EventTriggerBehavior>
                                    </interactivity:Interaction.Behaviors>
                                    <FlyoutBase.AttachedFlyout>
                                        <MenuFlyout>
                                            <MenuFlyoutItem x:Uid="MenuItemDelete" Text="Delete" Foreground="Red"
                                                            Command="{Binding DataContext.DeleteClickCommand, ElementName=Page}"
                                                            CommandParameter="{Binding}" />
                                        </MenuFlyout>
                                    </FlyoutBase.AttachedFlyout>
                                    <AppBarButton Icon="Play" IsCompact="True" Margin="0,-10,10,0"
                                                  Command="{Binding DataContext.EntryPlayClickCommand, ElementName=Page}"
                                                  CommandParameter="{Binding}"/>
                                    <TextBlock VerticalAlignment="Center" Grid.Column="1" Text="{Binding Name}"
                                               FontSize="30" FontFamily="Global User Interface"
                                               TextTrimming="CharacterEllipsis" FontWeight="Bold" />
                                </Grid>
                            </DataTemplate>
                        </ListView.ItemTemplate>
                    </ListView>
                </Grid>
            </PivotItem>
        </Pivot>
    </Grid>
    <Page.BottomAppBar>
        <CommandBar ClosedDisplayMode="Minimal">
            <AppBarButton x:Uid="CollectionAppBarNewPlaylist" Icon="Add" Label="New Playlist"
                          FontFamily="Global User Interface">
                <AppBarButton.Flyout>
                    <PickerFlyout x:Uid="PlaylistNewPicker" Title="NEW PLAYLIST" ConfirmationButtonsVisible="True"
                                  Placement="Full"
                                  Confirmed="PickerFlyout_Confirmed" Closed="PickerFlyout_Closed">
                        <PickerFlyout.Content>
                            <ListView ItemsSource="{Binding Service.Songs}"
                                      ItemTemplate="{StaticResource SongDataTemplate}"
                                      SelectionMode="Multiple" Padding="0,0,0,50">
                                <ListView.ItemContainerTransitions>
                                    <TransitionCollection>
                                        <AddDeleteThemeTransition />
                                        <ReorderThemeTransition />
                                    </TransitionCollection>
                                </ListView.ItemContainerTransitions>
                            </ListView>
                        </PickerFlyout.Content>
                    </PickerFlyout>
                </AppBarButton.Flyout>
            </AppBarButton>
            <CommandBar.SecondaryCommands>
                <AppBarButton Label="import music from phone" Command="{Binding ImportCommand}" />
                <AppBarButton Label="backup collection" Command="{Binding CreateBackupCommand}" />
                <AppBarButton Label="restore collection" Command="{Binding RestoreCommand}" />
            </CommandBar.SecondaryCommands>
        </CommandBar>
    </Page.BottomAppBar>
</audiotica:PageBase>